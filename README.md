# dwm

My build of dwm.

Patches used:
* attachasideandbelow, 
* autostart, 
* center,
* cfacts_bottomstack, 
* colorbar, 
* hide_vacant_tags, 
* movestack,
* pertag, 
* restartsig, 
* rotatestack,
* ru_gaps (tile, bottomstack, deck, grid, monocle), 
* scratchpad, 
* swallow, 
