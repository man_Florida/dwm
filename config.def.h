/* See LICENSE file for copyright and license details. */

/* I use a bunch of scripts, these must be on the system and sourced */

/*#define BROWSER "firefox"
#define BROWSER "librewolf"
#define BROWSER "firefox"
#define BROWSER2 "librewolf"
#define BROWSER3 "brave"
#define BROWSER4 "chromium"
#define FILEMANAGER "pcmanfm"
#define FILEMANAGER3 "st -e lf"
#define FILEMANAGER2 "st -e fff"
#define MATRIX "nheko"
#define MATRIX2 "st -e gomuks"
*/
#define TERMINAL "st"
#define TERMCLASS "St"
/*#define TERM2 "xterm"
#define XMPP "dino"
#define TERM2 "xterm"
#define XMPP "dino"
#define TERMINAL "st"
#define TERMCLASS "St"


appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const int gappx				= 10;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 1;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Roboto:size=10" ,"Noto Color Emoji:pixelsize=14:antialias=true:autohint=true" };
//static const char *fonts[]          = { "Liberation Sans:size=10" ,"Noto Color Emoji:pixelsize=14:antialias=true:autohint=true" };
//static const char *fonts[]          = { "Roboto Slab:size=10" ,"Noto Color Emoji:pixelsize=14:antialias=true:autohint=true" };
//static const char *fonts[]          = { "Roboto Slab:size=10" ,"JoyPixels:pixelsize=14:antialias=true:autohint=true" };
static const char dmenufont[]       = "Roboto:size=10";
//static const char col_gray1[]       = "#222222";
//static const char col_gray1[]       = "#282a36";
static const char col_gray1[]       = "#1e1c31";
//static const char col_gray1[]       = "#292d3e";
//static const char col_gray1[]       = "#282c34";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_gray5[]       = "#5b6268";
static const char col_blue[]        = "#51afef";
static const char col_cyan[]        = "#46d9ff";
static const char col_magenta[]     = "#c678dd";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm]	= { col_gray3,	col_gray1,	col_gray2 },
	[SchemeSel]		= { col_gray4,	col_gray1,	col_blue  },
	[SchemeStatus]  = { col_gray3,	col_gray1,  "#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]	= { col_gray4,	col_blue,	"#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
    [SchemeTagsNorm]= { col_gray5,	col_gray1,  "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
    [SchemeInfoSel]	= { col_gray4,	col_gray5,	"#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}
    [SchemeInfoNorm]= { col_gray3,	col_gray1,  "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
  /* Tags:
	  * 1: emacs, terminals
	  * 2: Browsers
	  * 3: Email
	  * 4: Chat apps
	  * 5: Bitwarden
	  */

	/* class					instance  title							tags mask	iscentered	isfloating  isterminal  noswallow  monitor */
	{ "Emacs",					NULL,     NULL,							1 << 0,		0,			0,          0,          -1,        -1 },
	{ "mpv",					NULL,     NULL,							1 << 0,		0,			0,          0,          -1,        -1 },
	{ "Firefox",				NULL,     NULL,							1 << 1,		0,			0,          0,          -1,        -1 },
	{ "Brave",					NULL,     NULL,							1 << 1,		0,			0,          0,          -1,        -1 },
	{ "Chromium",				NULL,     NULL,							1 << 1,		0,			0,          0,          -1,        -1 },
	{ "Librewolf",				NULL,     NULL,							1 << 1,		0,			0,          0,          -1,        -1 },
	{ "thunderbird",			NULL,     NULL,							1 << 2,		0,			0,          0,          -1,        -1 },
	{ "Mail",					NULL,     NULL,							1 << 2,		0,			0,          0,          -1,        -1 },
	{ "Signal",					NULL,     NULL,							1 << 3,		0,			0,          0,          -1,        -1 },
	{ "Dino",					NULL,     NULL,							1 << 3,		0,			0,          0,          -1,        -1 },
	{ "Session",				NULL,     NULL,							1 << 3,		0,			0,          0,          -1,        -1 },
	{ "Element",				NULL,     NULL,							1 << 3,		0,			0,          0,          -1,        -1 },
	{ "Bitwarden",				NULL,     NULL,							1 << 4,		0,			0,          0,          -1,        -1 },
	{ "Simplenote",				NULL,     NULL,							1 << 4,		0,			0,          0,          -1,        -1 },
	{ "St",						NULL,     NULL,							0,			0,			0,          1,           0,        -1 },
	{ NULL,						NULL,     "Event Tester",				0,			0,			0,          0,           1,        -1 }, /* xev */
	{ "Connman-gtk",			NULL,     NULL,							0,			1,			1,          0,           0,        -1 },
	{ "Nm-connection-editor",	NULL,     NULL,							0,			1,			1,          0,           0,        -1 },
	{ "tdrop",					NULL,     NULL,							0,			1,			1,          0,           0,        -1 },
	{ NULL,						NULL,     "Sign in to Security Device", 0,			1,			1,			0,           1,        -1 }, /* xev */
	{ NULL,						NULL,     "Unlock private key",			0,			1,			1,			0,           1,        -1 }, /* xev */
	{ NULL,						NULL,     "Unlock Keyring",				0,			1,			1,			0,           1,        -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.60; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals - CHANGED FOR XTERM/URXVT */

#include "gaplessgrid.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "tile",	tile },    /* first entry is default */
	{ "float",	NULL },    /* no layout function means floating behavior */
	{ "[M]",	monocle },
	{ "bstack", bstack },
	{ "horiz",  bstackhoriz },
	{ "grid",	gaplessgrid },
	{ "[D]",    deck },

	/*{ "[]=",      tile },
	{ "><>",      NULL },
	{ "[M]",      monocle },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "###",	  gaplessgrid },
	{ "[D]",      deck }, */

};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
//#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-p", "Commands", "-l", "6", NULL };
static const char *termcmd[]  = { TERMINAL, NULL };
//static const char *term2cmd[]  = { TERM2, NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { TERMINAL, "-t", scratchpadname, "-g", "120x34", NULL };

#include "shiftview.c"
#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY|ShiftMask,				XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,				XK_k,      movestack,      {.i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,	XK_j,      rotatestack,    {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,	XK_k,      rotatestack,    {.i = -1 } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_period, incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_comma,  incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ControlMask,           XK_h,      setcfact,       {.f = +0.25} },
	{ MODKEY|ControlMask,           XK_l,      setcfact,       {.f = -0.25} },
	{ MODKEY|ControlMask,           XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,						XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
    { MODKEY,                       XK_u,      setlayout,      {.v = &layouts[3]} },
    { MODKEY|ShiftMask,             XK_u,      setlayout,      {.v = &layouts[4]} },
    { MODKEY,						XK_g,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_d,      setlayout,      {.v = &layouts[6]} },
	{ MODKEY,                       XK_n,      shiftview,      { .i = +1 } },
	{ MODKEY|Mod1Mask,		        XK_n,      shiftview,      { .i = -1 } },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|Mod1Mask,				XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY|Mod1Mask,				XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ControlMask|Mod1Mask,  XK_Delete, quit,           {0} },
	{ MODKEY|ShiftMask,				XK_q,      quit,           {1} },
	{ MODKEY|ShiftMask,             XK_s,	   togglescratch,  {.v = scratchpadcmd } },
/*	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	{ Mod1Mask|ShiftMask,           XK_Return, spawn,          {.v = term2cmd } },

	{ ShiftMask|ControlMask,        XK_Delete, spawn,          SHCMD("dmenu-locksuspend.sh") },
	{ Mod1Mask|ControlMask,			XK_Delete, spawn,          SHCMD("dm-logout.sh") },
	{ MODKEY|ShiftMask,             XK_b,      spawn,          SHCMD("firejail " BROWSER3 " --incognito") },
	{ Mod1Mask|ShiftMask,           XK_b,      spawn,          SHCMD(BROWSER4 " --incognito") },
	{ ControlMask|ShiftMask,        XK_b,      spawn,          SHCMD(BROWSER4 " --incognito") },
	{ Mod1Mask|ShiftMask,           XK_c,      spawn,          SHCMD(TERMINAL " calcurse") },
//	{ MODKEY|ShiftMask,             XK_e,      spawn,          SHCMD("element-script.sh") },
	{ MODKEY|ShiftMask,             XK_e,      spawn,          SHCMD("emacsclient -c -a 'emacs'") },
	{ MODKEY|ShiftMask,             XK_f,      spawn,          SHCMD(FILEMANAGER) },
	{ Mod1Mask|ShiftMask,			XK_f,      spawn,          SHCMD(FILEMANAGER2) },
	{ MODKEY|Mod1Mask,				XK_f,      spawn,          SHCMD(FILEMANAGER3) },
	{ MODKEY|ShiftMask,             XK_i,      spawn,          SHCMD("connman-script.sh") },
	{ MODKEY|ShiftMask,             XK_l,      spawn,          SHCMD(TERMINAL " -e lynx") },
	{ MODKEY|ShiftMask,             XK_m,      spawn,          SHCMD(MATRIX) },
	{ Mod1Mask|ShiftMask,           XK_m,      spawn,          SHCMD(MATRIX2) },
	{ MODKEY|ShiftMask,             XK_n,      spawn,          SHCMD(TERMINAL " -e newsboat") },
	{ MODKEY,						XK_o,      spawn,          SHCMD("dmenu-clipmenu.sh") },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          SHCMD("dmenu-desktop.sh") },
	{ Mod1Mask,						XK_p,      spawn,          SHCMD("picom-toggle.sh") },
	{ MODKEY|ShiftMask,             XK_t,      spawn,          SHCMD("mail-script.sh") },
	{ MODKEY|Mod1Mask,              XK_t,      spawn,          SHCMD("tutanota-script.sh") },
	{ MODKEY,						XK_w,      spawn,          SHCMD("dmenu-wiki.sh") },
	{ MODKEY|ShiftMask,             XK_w,      spawn,          SHCMD("firejail --noprofile " BROWSER " --private") },
	{ Mod1Mask|ShiftMask,           XK_w,      spawn,          SHCMD("firejail --noprofile " BROWSER2 " --private") },
	{ MODKEY|ShiftMask,             XK_x,      spawn,          SHCMD(XMPP) },

	{ Mod1Mask,						XK_j,      spawn,          SHCMD("volume_adjust.sh 3%-") },
	{ Mod1Mask,						XK_k,      spawn,          SHCMD("volume_adjust.sh 3%+") },
	{ Mod1Mask,						XK_m,      spawn,          SHCMD("volume_adjust.sh toggle") },
	{ Mod1Mask|ControlMask,			XK_j,      spawn,          SHCMD("mic_adjust.sh 3%-") },
	{ Mod1Mask|ControlMask,			XK_k,      spawn,          SHCMD("mic_adjust.sh 3%+") },
	{ Mod1Mask|ControlMask,			XK_m,      spawn,          SHCMD("mic_adjust.sh toggle") },
	{ Mod1Mask,						XK_o,      spawn,          SHCMD("brightness_adjust.sh -set 100") },
	{ Mod1Mask,						XK_y,      spawn,          SHCMD("brightness_adjust.sh -set 00") },
	{ Mod1Mask,						XK_i,      spawn,          SHCMD("brightness_adjust.sh -inc 10") },
	{ Mod1Mask,						XK_u,      spawn,          SHCMD("brightness_adjust.sh -dec 10") },
*/
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

